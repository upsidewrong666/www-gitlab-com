---
layout: markdown_page
title: "Technical Account Management"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, implementation engineers and support. 

See the [Technical Account Manager role description](/roles/sales/technical-account-manager/) for further information.

### Advocacy
A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product.  In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

### Values

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption, training and regular healthchecks.

### Responsibilities and Services
All Premium customers with a minimum ARR of $100,000 are aligned with a Technical Account Manager. There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

#### Relationship Management
* Regular cadence calls
* Regular open issue reviews and issue escalations
* Account healthchecks
* Quarterly business reviews
* Success strategy roadmaps - beginning with a 30/60/90 day success plan
* To act as a key point of contact for guidance, advice and as a liason between the customer and other GitLab teams
* Own, manage, and deliver the customer onboarding experience
* Help GitLab's customers realize the value of their investment in GitLab
* GitLab Days

#### Training
* Identification of pain points and training required
* Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
* "Brown Bag" trainings
* Regular communucation and updates on GitLab features
* Product and feature guidance - new feature presentations

#### Support
* Upgrade planning
* User adoption strategy
* Migration strategy and planning
* Launch support
* Monitors support tickets and ensures that the customer receives the appropriate support levels
* Support ticket escalations

It is also possible for a customer to pay for a Technical Account Manager's services in order to receive priority, "white glove" assistance, guidance and support as well as more time allocated to their account on a monthly basis. There are also additional services a Technical Account Manager will provide to the services listed above. These are currently to be determined and will become available before the end of the second quarter of 2018.

---

# Engagement Models
There are three models currently offered for Technical Account Manager engagement. These are broken into tiers that currently use Annual Recurring Revenue as a metric for determining a manageable volume for a single Technical Account Manager and the depth of involvement during the engagement.

## Table of Technical Account Manager Engagements
Technical Account Managers focus on [large and strategic accounts](/handbook/sales/#market-segmentation), [small business (SMB) and mid-market accounts](/handbook/sales/#market-segmentation) are typically handled by [Account Managers](/handbook/customer-success/account-management). 

||TIER 1 |TIER 2 |TIER 3 |
|:---|:---|:---|:---|
|Minimum ARR|$500,000|$250,000|$100,000|

## Managing the Customer Engagement
Technical Account Managers will typically manage customer engagements via a GitLab project in the [`account-management` group](https://gitlab.com/gitlab-com/account-management/). This project will be based off a [customer collaboration project template](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) and then customized to match the customer's needs as outlined above. The project is pre-loaded with milestones, issues, labels and a README template to help kick off the project and outline a proof of concept, implementation and customer onboarding.

### To start a new customer engagement:
1.  Somewhere between step 3 and step 7 of the customer journey sequence, a Solutions Architect should create a project for the customer in GitLab and include an Implementation Engineer and Technical Account Manager who're best aligned with the customer account.
2. Go to the [customer collaboration project template project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template).
3. Follow the steps in the PLEASE-READ-THESE-INSTRUCTIONS.md file

### Where does a Technical Account Manager fit in?
During the pre-sales process, a Solutions Architect owns the project with assistance from the Strategic Account Leader and should include the Implementation Engineer if there is one assigned. During the pre-sales project, a Technical Account Manager is involved but only for visibility. Until the account becomes a paying customer, the project remains in pre-sales. Once the customer has paid, the Strategic Account Leader will set up the Welcome to GitLab call along with the key GitLab employees (SAL, SA, IE and Technical Account Manager) and the customer. There is a preloaded issue for this in the project template.

This call will introduce the customer to the Technical Account Manager and begin the handover process. The Technical Account Manager will then lead the rest of the call and own the project going forward. The project is then moved from a pre-sales project under the [`pre-sales account-management` group](https://gitlab.com/gitlab-com/account-management/pre-sales) to a post-sales project under [`account-management` group](https://gitlab.com/gitlab-com/account-management).

Typically this project template isn't used for small business or mid-market accounts. However, if an Account Manager feels this could be useful to assist their relationship with the customer, then they can utilise it and edit down as needed. 

### Salesforce - Customer Success Section

On an account view in Salesforce, there is a Customer Success section, with the following fields:

* Health Score - A field to record the overall customer health (details TBD on this)
* GitLab Customer Success Project - Where you enter the URL of the project created using the template described above
* Customer Slack Channel - A field to record Slack channel(s) used for internal and external customer collaboration. If a channel is internal, make sure it follows the naming convention"#organisation-name-internal"
* Solutions Architect - The Solutions Architect aligned with the acccount
* Technical Account Manager	- The Technical Account Manager aligned with the acccount

### Salesforce - Customer Success Automations

* New Zendesk Ticket Notifications - For all new Zendesk tickets that are created, the corresponding TAM for the account that the ticket is associated with will receive an email notification alerting them of a new ticket. This currently is a one time notification that only occurs when the Zendesk ticket is first created in Salesforce.

## Salesforce - Tracking and Use of Custom Salesforce Objects

### Executive Business Reviews (EBRs) - Tracking and Use in Salesforce

In order to appropriately track and create Executive Business Review Objects please follow the instructions below:

Creating the first EBR for an account:
* If it is the first EBR for an account you can navigate to the EBR Tab. This can be located on the either the 'Sales' or the 'Service' App within Salesforce
* On the top of the list shown, you click on the 'New' button. This will enable you to create the new EBR object where you can provide the following. `Executive Business Review Name` is the name that you give this EBR. Select the `EBR Date` that the EBR is schedule to occur on. If the EBR Date changes please see below for handiling EBR Date Changes To relate the EBR to the appropriate account using the search functionality provided by the `Account` field. `EBR Status` should be set to either 'Not Started' or 'Scheduled' depending on the current state of the EBR. All other fields should be left alone. Please refer to the section below on if you should link the EBR to any Opporutnities.  

Creating any successive EBR's for an account and haddiling Declined/Cancelled EBR's:
* In order to create any successive EBR's (anything besides the first EBR), DO NOT us the the 'New' button as this will cause errors with tracking. Instead after the completion, cancellation or declined invite of an EBR enter into the current EBR and Update the `EBR Success` to the appropriate value. This will automatically create the next EBR for this account 90 days after the `EBR Date` for the current EBR (ensuring that there is 1 EBR per quarter). A number of fields will also auto populate making the creation process of new EBR's much easier and quicker. 

Tracking the performance of EBR's:
* There are currently two picklist fields in order to provide tracking for EBR's
* `EBR Status` - This field denotes if the status of scheduling of the EBR. This should be used to help monitor workflow (Scheduled vs Not Started) and also to track cancellations, declined EBR's as well as completed EBR's that were actually held
* `EBR Success` - This field tracks the overall success of the EBR with the default set to `Incomplete`. This field should only be updated either once the EBR is held or after a cancellation or declined EBR. This tracks what the TAM believes was the outcome of the EBR (or if it was declined or Cancelled)
* `Declined` vs `Cancelled` - The main differnce between a Declined EBR and a Cancelled EBR is dependant on how it is handeled. EX: If a customer pushes to only have 2 EBR's a year than 2 of the EBR's each year should be labeled as Declined. Wehre as if an EBR in a the third Q is cancelled last minute and the customer decides not to reschedle and instead to sync next Q than that EBR was cancelled. 

Handiling EBR Date Changes:
* If the `EBR Date` is changed and is still planned to occur within the same fiscal quarter that it was originally planned to take place during simply update the `EBR Date` field to the new scheduled date
* If the `EBR Date` is changed to a date that is in the next fiscal quarter please treat it as if that EBR was declined. Update the `EBR Status` and the `EBR Success` field to `Declined` and save the EBR. Please review the section above on creating successive EBR's for an account as it will automatically be created.  

Linking EBR's to Opportunities: 
* Although all EBR's can be associated to an Opportunity All EBR's should not be associated to an opportunity. Only the 4 latest EBRS should be related to an upcoming opportunity. Ex: If there is an upcoming opportunity in Q4 2019 then the only EBR's that should be related to this Opportunity should be the following EBR's, Q1-2019-EBR, Q2-2019-EBR, Q3-2019-EBR & Q4-2019-EBR (Assuming that the Q4 EBR took place before the opportunity close date)
* To link an Opportunity to and EBR use the "New EBR-Opportunity Associations" Button that is located above the "EBR-Opportunity Associations" Related List. This is done on either the Opporunity or the EBR that you are attempting to associate with one another. Scroll down to find the "EBR-Opportunity Associations" related list on the layout and click on "New EBR-Opportunity Associations" Button. From there search and select the appropriate Opportunity and EBR that need to be associated with one another and save. 

### Statement of Works (SOWs) - Tracking and Use in Salesforce

Statement of Works in Salesforce are used to track the progress of our SOW's that are agreed upon with our customers. These SOW's describe the professional services that Gitlab will deliver to our client. As this is related to a billable service that we are extending to our clients all SOW's must be related to an Opporutnity and an Account. The Opportunity that each SOW is associated with will house information relevant to the Opportunity (Amount, IACV etc.) while the Statement of Work will house notes, details and monitor the progress of the SOW (Go Live Date, Kick Off Date etc.). If a client would like to move forward with many professional services at once then all of these services would be encapsulated and related through one Opportunity and one SOW. If an existing client, who previously purchased professional services from Gitlab, would like to purchase addition professional services than a new Opportunity and SOW would be created in Salesforce. Review our section in the [handbook](/handbook/sales/#when-to-create-an-opportunity) about creating new opportunities if you have any questions around creating an opportunity.  

In order to track the contacts that are associated with a SOW utilize the SOW-Contact Association list. This can be accessed by navigating to the SOW page layout and locating the SOW-Contact Association related list. From there you can create a new association by looking up the contact that is associated with this SOW. Multiple contacts can be associated with a single SOW. 

## The Customer Meta-Record
The concept of a customer meta-record is how a Technical Account Manager develops and maintains a holistic understanding of customer accounts.  It is a living record that includes both business and technical information about the customer.  The data captured here informs not only the Technical Account Manager, but all of GitLab about critical details to the success of our customers.

## Business Profile
The business profile is captured during the discovery and scoping stages in the pre-sales phase of the customner lifecycle.  Once an opportunity is set to closed-won, the Technical Account Manager is largely responsible for maintaining this data. The business profile of a customer's meta-record contains data points related to their organizational structure, the internal advocate/champion, the features most compelling to the customer, the "why" of purchasing GitLab, objectives for the implementation and critically, the data captured in the customer feedback loop.

## Technical Profile
The technical profile includes objective data points about a customers technical landscape and can be captured at any stage of the customer lifecycle. Specifically, items related to architecture, sizing, scale, security and compliance requirements are captured in the technical profile. In general, solution architects and implementation specialists are primarily responsible for collecting and capturing information for the technical profile. This information is transitioned to the Technical Account Manager once a customer goes live and the Technical Account Manager maintains the information throughout the remainder of the customer lifecycle.

## Automated Customer Discovery Tools
A series of data sheets and automation scripts is being developed that will streamline much of the intake, discovery and reconnaissance activities during the pre-sales stages of the customer lifecycle.  Decisions surrounding where this data will reside and which information is part of the critical path during customer onboarding are currently underway.
